﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WSConvertisseur.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WSConvertisseur.Controllers
{
    [Route("api/Devise")]
    [ApiController]
    public class DeviseController : ControllerBase
    {
        private List<Devise> devises { get; set; }

        public DeviseController()
        {
            devises = new List<Devise>();
            devises.Add(new Devise() { Id = 1, NomDevise = "Dollar", Taux = 1.08 });
            devises.Add(new Devise() { Id = 2, NomDevise = "Franc Suisse", Taux = 1.07 });
            devises.Add(new Devise() { Id = 3, NomDevise = "Yen", Taux = 120 });
        }

        /// <summary>
        /// Get all currency
        /// </summary>
        /// <returns>Http response</returns>
        /// <response code="200">When the currencies are found</response> 
        /// <response code="404">When the currencies are not found</response>
        // GET: api/<DeviseController>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Devise>), 200)]
        public IActionResult GetAll()
        {
            return Ok(this.devises);
        }

        /// <summary>
        /// Get a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="id">The id of the currency</param>
        /// <response code="200">When the currency id is found</response> 
        /// <response code="404">When the currency id is not found</response>
        // GET api/<DeviseController>/5
        [ProducesResponseType(typeof(Devise), 200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}", Name = "GetDevise")]
        public IActionResult GetById(int id)
        {
            Devise devise = this.devises.FirstOrDefault(devise => devise.Id == id);
            if (devise == null)
            {
                return NotFound();
            }
            return Ok(devise);
        }

        /// <summary>
        /// Add a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="id">The json of a devise</param>
        /// <response code="201">When the currency id is added</response> 
        /// <response code="400">When the currency id is not added</response>
        // POST api/<DeviseController>
        [HttpPost()]
        [ProducesResponseType(typeof(Devise), 201)]
        [ProducesResponseType(400)]
        public IActionResult Post([FromBody]Devise devise)
        {
            if (!ModelState.IsValid) { 
                return BadRequest(ModelState); 
            }
            devises.Add(devise); 
            return CreatedAtRoute("GetDevise", new { id = devise.Id }, devise);
        }

        /// <summary>
        /// Update a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="devise">devise to update in json</param>
        /// <response code="204">When the currency id is updated</response> 
        /// <response code="400">When the currency id is not updated</response>
        // PUT api/<DeviseController>/5
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public IActionResult Put(int id, [FromBody] Devise devise)
        {
            if (!ModelState.IsValid) { 
                return BadRequest(ModelState); 
            }
            if (id != devise.Id) { 
                return BadRequest(); 
            }
            int index = devises.FindIndex((d) => d.Id == id); if (index < 0) { 
                return NotFound(); 
            }
            devises[index] = devise; 
            return NoContent();
        }

        /// <summary>
        /// Remove a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="id">The id of the currency to delete</param>
        /// <response code="200">When the currency id is deleted</response> 
        /// <response code="404">When the currency id is not deleted</response>
        // DELETE api/<DeviseController>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Devise), 200)]
        [ProducesResponseType(404)]
        public IActionResult Delete(int id)
        {
            Devise devise = devises.FirstOrDefault((d) => d.Id == id);
            if (devise == null) { 
                return NotFound(); 
            }
            devises.Remove(devise);
            return Ok(devise);
        }
    }
}
